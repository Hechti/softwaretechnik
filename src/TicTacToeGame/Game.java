package TicTacToeGame;

public class Game {
	
	private char[][] gameBoard;
	private static char playingPlayer;
	
	public Game(){
		gameBoard  = new char[3][3];
		playingPlayer = 'X';
		initGameBoard();
	}
	
	public void initGameBoard(){
		for(int i=0; i<3; i++){
			for(int j=0; j<3; j++){
				gameBoard[i][j] = '-';
			}
		}
	}
	
	public void printGameBoard(){
		for(int i=0; i<3; i++){
			System.out.print('|');
			for(int j=0; j<3; j++){
				System.out.print(gameBoard[i][j]);	
			}
			System.out.println('|');
		}	
		System.out.println("#####");
	}
	
	public void setPlayerMarker(int i, int j){
		int x = i - 1;
		int y = j - 1;
		if(gameBoard[x][y] == '-'){
			gameBoard[x][y]= playingPlayer;
		}
	}
	
	public void changePlayer(char i){
		if(i == 'X'){
			playingPlayer = 'O';
		}
		else{
			playingPlayer = 'X';
		}
	}
	
	
	public boolean isGameOver(){
		if(checkRowsWin() || checkColumnsWin() || checkDiagonalsWin()){
			System.out.println("The winner is: "+ playingPlayer);
			return true;
		}else if(isGameBoardFull()){
			System.out.println("The game is over! DRAW");
			return true;
		}else{
			return false;
		}
	}
	
	public char getPlayingPlayer(){
		return playingPlayer;
	}
	
	public boolean isGameBoardFull(){
		boolean isFull = true;
		for(int i=0; i<3; i++){
			for(int j=0; j<3; j++){
				if(gameBoard[i][j] == '-'){
					isFull = false;
				}
			}
		}
		return isFull;
	}
	
	public boolean checkRowsWin(){
		for(int i = 0; i < 3; i++){
			if(checkThreeFields(gameBoard[0][i],gameBoard[1][i],gameBoard[2][i])){
				return true;
			}
		}
		return false;
	}
	
	public boolean checkColumnsWin(){
		for(int i = 0; i < 3; i++){
			if(checkThreeFields(gameBoard[i][0],gameBoard[i][1],gameBoard[i][2])){
				return true;
			}
		}
		return false;
	}
	
	public boolean checkDiagonalsWin(){
		if(checkThreeFields(gameBoard[0][0],gameBoard[1][1],gameBoard[2][2]) || checkThreeFields(gameBoard[0][2],gameBoard[1][1],gameBoard[2][0])){
			return true;
		}else {
			return false;
		}
	}
	
	
	public boolean checkThreeFields(char i, char j, char k){
		if((i != '-') && (i == j) && (j == k)){
			return true;
		}else{
			return false;
		}
	}
	
	
	
	
	
	
}
